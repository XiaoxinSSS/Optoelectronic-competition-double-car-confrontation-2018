# Untitled - By: Hong - 周二 11月 21 2017
import pyb
import sensor, time
from pyb import Servo
class PID(object):
    kp = ki = kd = 0
    def __init__(self,p ,i ,d):
        self.kp = float(p)
        self.ki = float(i)
        self.kd = float(d)
    def pidvalue_1(self,Delay_1,Delay_2,blobs,sum_error,last_error,derror):
        error = 151 - blobs
        sum_error = sum_error+error
        derror = error -last_error
        pidvalue = self.kp*error+self.ki*sum_error+self.kd*derror
        pidvalue = - pidvalue
        last_error = error
        print('pid',pidvalue)
        print('e',error)
        s1 = Servo(1)
        if(pidvalue>=0):
            if(pidvalue>=18):
                s1.angle(18)
                pyb.delay(30+Delay_1)
                return((0,sum_error,derror,last_error))
            if(pidvalue <18):
                s1.angle(pidvalue)
                pyb.delay(30)
                flag = pidvalue
                return((flag,sum_error,derror,last_error))
        else:
            if(pidvalue<=(-18)):
                s1.angle(-18)
                pyb.delay(30)
                return((0,sum_error,derror,last_error))
            if(pidvalue >(-18)):
                s1.angle(pidvalue)
                pyb.delay(30)
                flag = pidvalue
                #print('flag',flag)
                return((flag,sum_error,derror,last_error))


