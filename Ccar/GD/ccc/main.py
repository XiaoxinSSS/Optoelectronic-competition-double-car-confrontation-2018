import pyb
from pid import PID
from turn import TURN
from pyb import Pin
import sensor, image, time
from pyb import Servo
error = 0
sum_error = 0
derror = 0
last_error = 0
kp = 0.06
ki = 0
kd = 0.0
flag = (0,0,0,0)
time_1 = 10   #找灯冲延时
length_y = 110  #Turnning 115稳定距离
Delay_1 = 0    #限幅打死延时
Delay_2 = 0    #pid舵机延时
Delay_3 = 1000  #靠近飘逸延时
Red_threshold = (40, 100, 43, 76, -2, 39)
#ROI = (100,0 ,44 0,320)
GRAYSCALE_1 = (200, 255)#(116, 188)#(102, 184)#(92, 149)#(99, 173)#(175, 230)
sensor.reset()
sensor.set_pixformat(sensor.GRAYSCALE)
sensor.set_framesize(sensor.QVGA)
sensor.skip_frames(time = 2000)
#sensor.set_windowing(ROI)
sensor.set_auto_whitebal(False)
out_1 = Pin('P4',Pin.OUT_PP)#灭灯
out_2 = Pin('P5',Pin.OUT_PP)
clock = time.clock()
out_3 = Pin('P6',Pin.OUT_PP)#
out_4 = Pin('P8',Pin.OUT_PP)
out_5 = Pin('P3',Pin.OUT_PP)
out_6 = Pin('P0',Pin.OUT_PP)
in_2_9 = Pin('P9',Pin.IN)
in_3_P2 = Pin('P2',Pin.IN)
in_4_P1 =  Pin('P1',Pin.IN)

while(True):
    #clock.tick()
    #in_1 = Pin('P0', Pin.IN)#左1
    #in_2 = Pin('P1', Pin.IN)#左2
    #in_3 = Pin('P2', Pin.IN)#右1
    #in_4 = Pin('P3', Pin.IN)#右2
    #value1 = in_1.value()
    #value2 = in_2.value()
    #value3 = in_3.value()
    #value4 = in_4.value()
    #print(value1,value3)
    in_3_P2_value = in_3_P2.value()
    if(in_3_P2_value == 1):
        s1 = Servo(1)
        s1.angle(-1)
    else:
        #in_1_value = in_1_s.value()
        #print(in_1_value)
        #if(in_1_value == 0):
            #print("A")
        in_4_P1_value = in_4_P1.value()
        if(in_4_P1_value == 1):
            #print("AAAAAAA")
            pass
        else:
            img = sensor.snapshot()
            #img = img.binary([GRAYSCALE_1])
            blobs = img.find_blobs([GRAYSCALE_1])
            #print(blobs)
            pixs = []
            if blobs:
                out_5.high()
                for blob in blobs:
                    pixs.append(blob[4])
                m = max(pixs)
                i = pixs.index(m)
                length_1 = blobs[i][6]
                #print("#########")
                #print(blobs[i][6])
                #print("#########")
                if(length_1 > 207):
                    out_6.high()
                if (length_1 < 55):
                    #print("CCCCCCCC")
                    out_4.high()
                    out_1.low()
                    out_2.low()
                    out_6.low()
                    find = PID(kp, ki, kd)
                    flag = find.pidvalue_1(Delay_1, Delay_2, blobs[i][5], sum_error, derror, last_error)
                    sum_error = flag[1]
                    derror = flag[2]
                    last_error = flag[3]
                    #print(blobs[i][6])
                elif (length_1<length_y):    #175
                    #print("DDDDDDDD")
                    out_4.low()
                    out_1.low()
                    out_2.low()
                    out_6.low()
                    find = PID(kp,ki,kd)
                    flag = find.pidvalue_1(Delay_1,Delay_2,blobs[i][5],sum_error,derror,last_error)
                    #print(flag)
                    sum_error = flag[1]
                    derror =flag[2]
                    last_error = flag[3]
                    #print(blobs[i][6])
                else:
                    #print("EEEEEEE")
                    out_4.low()
                    out_1.high()
                    out_6.low()
                    find = PID(kp,ki,kd)
                    flag = find.pidvalue_1(Delay_1,Delay_2,blobs[i][5],sum_error,derror,last_error)
                    sum_error = flag[1]
                    derror =flag[2]
                    last_error = flag[3]
                    #print(blobs[i][6])
                    # if(length_1 > 305):
                    #     out_2.high()
                    # else:
                    #     out_2.low()

            else:
                #print("B")
                out_5.low()
                out_4.low()
                out_1.low()
                out_2.low()
                out_6.low()
                s1 = Servo(1)
                in_2_9_value = in_2_9.value()
                #print(in_2_9_value)
                if(in_2_9_value == 1):
                    while 1:
                        out_2.low()
                        s1.angle(-17)
                        img = sensor.snapshot()
                        blobs = img.find_blobs([GRAYSCALE_1])
                        if blobs:
                            out_3.high()
                            break
                        else:
                            out_3.low()
                else:
                    while 1:
                        out_2.high()
                        s1.angle(17)
                        img = sensor.snapshot()
                        blobs = img.find_blobs([GRAYSCALE_1])
                        if blobs:
                            out_3.high()
                            break
                        else:
                            out_3.low()
        #print(clock.fps())
