 #include <Servo.h>     //速度1：7.1
Servo servos;
int pwm1 = 3;
int pwm2 = 4;
int pwm3 = 5;
int pwm4 = 6;

int out_1 = 40;
int out_2 = 42;
int out_3 = 44;
int out_4 = 46;
int out_5 = 38;
int huang_2 = 50;
int huang_1 = 48;
int read_image = 34;
int back = 36;

int flag = 1;
int count = 0;
int signal_read = 0; 
int set_speed = 0;   

int left_speed_actual = 0,right_speed_actual = 0;
double  Kp = 13,Ki = 0,Kd = 0;

double  left_set_speed = 0;
double  left_error = 0,left_integral = 0,left_derivative = 0;
double  left_last_error = 0;
double  left_pid_value = 0;

double  right_set_speed = 0;
double  right_error = 0,right_integral = 0,right_derivative = 0;
double  right_last_error = 0;
double  right_pid_value = 0;

void read_speed()
{
  if(Serial2.available() > 0)
  {
     left_speed_actual = Serial2.read() * 0.3;
//     Serial.print(left_speed_actual);
//     Serial.print("   ");
  }  
  if(Serial3.available() > 0)
  {
      right_speed_actual = Serial3.read() * 0.3;  
//      Serial.println(right_speed_actual);
  }
}

void left_pid_cal()
{
   left_error = left_set_speed - left_speed_actual;
   left_integral = left_integral + left_error;
   left_derivative = left_error - left_last_error;
   left_pid_value = Kp*left_error +Ki*left_integral + Kd*left_derivative;
   left_last_error = left_error;  
   if(left_pid_value >= 120)
   {
       left_pid_value = 120;
   }
   if(left_pid_value <= 0)
   {
       left_pid_value = 0;
   }
}

void right_pid_cal()
{
   right_error = right_set_speed - right_speed_actual;
   right_integral = right_integral + right_error;
   right_derivative = right_error - right_last_error;
   right_pid_value = Kp*right_error + Ki*right_integral + Kd*right_derivative;
   right_last_error = right_error;  
   if(right_pid_value >= 120)
   {
       right_pid_value = 120;
   }
   if(right_pid_value <= 0)
   {
       right_pid_value = 0;
   }
}

void set_motor_pid(int left_speed,int right_speed)
{   
    left_set_speed = left_speed;
    right_set_speed = right_speed;
    read_speed();
    left_pid_cal();
    right_pid_cal();
//    Serial.print(left_pid_value);
//    Serial.print("   ");
//    Serial.println(right_pid_value);
    if (right_pid_value >= 0 && left_pid_value >= 0) 
    {
      analogWrite(pwm1,left_pid_value);
      analogWrite(pwm2, 0);
      analogWrite(pwm3, right_pid_value);
      analogWrite(pwm4, 0);
    }  
    else if (right_pid_value >= 0 && left_pid_value < 0)   
    {
      left_pid_value = -left_pid_value;
      analogWrite(pwm1, 0);
      analogWrite(pwm2, left_pid_value);
      analogWrite(pwm3, right_pid_value);
      analogWrite(pwm4, 0);
    }
    else if (right_pid_value < 0 && left_pid_value >= 0) 
    {
      right_pid_value = -right_pid_value;
      analogWrite(pwm1, left_pid_value);
      analogWrite(pwm2, 0);
      analogWrite(pwm3, 0);
      analogWrite(pwm4, right_pid_value);
    }
    else if (right_pid_value < 0 && left_pid_value < 0) 
    {
      right_pid_value = - right_pid_value;
      left_pid_value = - left_pid_value;
      analogWrite(pwm1, 0);
      analogWrite(pwm2, left_pid_value);
      analogWrite(pwm3, 0);
      analogWrite(pwm4, right_pid_value);
    }
}

void set_motor(int left_speed,int right_speed)
{
    if (right_speed >= 0 && left_speed >= 0) 
    {
      analogWrite(pwm1,left_speed);
      analogWrite(pwm2, 0);
      analogWrite(pwm3, right_speed);
      analogWrite(pwm4, 0);
    }  
    else if (right_speed >= 0 && left_speed < 0)   
    {
      left_speed = -left_speed;
      analogWrite(pwm1, 0);
      analogWrite(pwm2, left_speed);
      analogWrite(pwm3, right_speed);
      analogWrite(pwm4, 0);
    }
    else if (right_speed < 0 && left_speed >= 0) 
    {
      right_speed = -right_speed;
      analogWrite(pwm1, left_speed);
      analogWrite(pwm2, 0);
      analogWrite(pwm3, 0);
      analogWrite(pwm4, right_speed);
    }
    else if (right_speed < 0 && left_speed < 0) 
    {
      right_speed = -right_speed;
      left_speed = -left_speed;
      analogWrite(pwm1, 0);
      analogWrite(pwm2, left_speed);
      analogWrite(pwm3, 0);
      analogWrite(pwm4, right_speed);
    }
}

void setup() {
  // put your setup code here, to run once:
  pinMode(pwm1,OUTPUT);
  pinMode(pwm2,OUTPUT);
  pinMode(pwm3,OUTPUT);
  pinMode(pwm4,OUTPUT);
  pinMode(32,INPUT);   // P0
  pinMode(huang_2,INPUT);   //黄管2
  pinMode(read_image,OUTPUT);  // P1 控制摄像头是否读图像  
  pinMode(huang_1,INPUT);  //黄管1
  pinMode(out_1,INPUT); //out_1  P4 40  第二段减速  
  pinMode(out_2,INPUT);   //out_2  P5 42 自传方向 0的时候-12度转 1的时候12度转  
  pinMode(out_3,INPUT);   // out_3  P6 44自传有图像的时候跳出循环  
  pinMode(out_5,INPUT);  //out_5  P3 46有图像为1  没图像为0  
  pinMode(out_4,INPUT);  //out_4 P8 48第一段减速  
  pinMode(back,OUTPUT);  // P2 控制倒退的时候舵机打直
  servos.attach(7);
  servos.write(63);   //初始化60度舵机
  digitalWrite(back,LOW);
  digitalWrite(read_image,LOW);
  Serial.begin(9600);
  Serial2.begin(9600);
  Serial3.begin(9600);
}

void loop() {
  // put your main code here, to run repeatedly:
  if(digitalRead(out_5) == 0)     //自传找灯
  {
    while(1)
      {   
        if(digitalRead(out_2) == 1)
        {
          set_motor(-130,-5);
        }
        else
        {
          set_motor(-5,-130);
        }
//        Serial.println(digitalRead(44));
        if(digitalRead(out_3) == 1)
        {
           set_motor(0,0);
           delay(20);
           break;  
        }
      }
  }
  else{
//    Serial.print(digitalRead(48));
    if(digitalRead(out_4) == 1)
    {
//    Serial.println("AAAAA");
      set_motor_pid(73,73);
      read_speed();
      count = 1;
    }
    else
    {
      if((count == 1) && (left_speed_actual > 52))
      {
        set_motor(0,0);
        delay(20);
        set_motor(-120,-120);
        delay(left_speed_actual * 3.3 );    //2.5
        set_motor(0,0);
        delay(20);
        count = 0;  
      }
      set_motor_pid(45,45);
//      Serial.println("42");
      signal_read = digitalRead(out_1);
//      Serial.println(signal_read);
      if(signal_read == 1)
      {
        set_motor(0,0);
        delay(20);
        read_speed();
        set_motor(-120,-120);
        delay(left_speed_actual * 8.2 );  //7.6
        set_motor(0,0);
        delay(20);
        while(1)
        {
          set_motor_pid(8,8);
//          Serial.println("8");
          if((digitalRead(out_1) == 0) || (digitalRead(out_4) == 1) || (digitalRead(out_3) == 0))
          {
            break;
          }
//          Serial.println(digitalRead(38));
          if((digitalRead(huang_1) == 0) || (digitalRead(huang_2) == 0) || (digitalRead(32) == 1))
          { 
            digitalWrite(read_image,HIGH);
            set_motor(0,0);
            delay(20);
            set_motor(-100,-100);
            delay(70);  
            set_motor(0,0);         
            servos.write(165);
            delay(450);
            servos.write(60);
            delay(120);
            digitalWrite(back,HIGH);
            set_motor(-95,-89);
            delay(1200);
            digitalWrite(back,LOW);
            digitalWrite(read_image,LOW);
            set_motor(0,0);
            delay(20);
//            Serial.print("FFFFFF");
            break;
          }
        }
      }
    }
  }
}

