import pyb
from pyb import Pin
import sensor, image, time
from pyb import Servo

flag = (0,0,0,0)

#Red_threshold = (40, 100, 43, 76, -2, 39)
ROI = (0,170,640,310)
GRAYSCALE_1 = (180, 255)#(116, 188)#(102, 184)#(92, 149)#(99, 173)#(175, 230)
sensor.reset()
sensor.set_pixformat(sensor.GRAYSCALE)
sensor.set_framesize(sensor.VGA)
sensor.skip_frames(80)
sensor.set_windowing(ROI)
sensor.set_auto_whitebal(False)
out_1 = Pin('P4',Pin.OUT_PP)#灭灯
while(True):
    img = sensor.snapshot()
    img = img.binary([GRAYSCALE_1])
    blobs = img.find_blobs([GRAYSCALE_1])
    #print(blobs)
    if blobs:
        out_1.high()
    else:
        out_1.low()

