# -*- coding: utf-8 -*-
# UART: pi  USBtoTTL to  arduino mega 2560
# 2018.7.1 GS

import serial
import time

ser = serial.Serial('/dev/ttyUSB0',9600,timeout=1)

class switch(object):
	def __init__(self):
		self.value = value
		self.fall = False
    
    def __iter__(self):
    	yield self.match
    	raise StopIteration

    def match(self,*args):
    	if self.fall or not args:
    		return True

    	elif self.value in args:
    		self.fall = True
    		return True

    	else:
    		return False

def x_to_angle():
	if((x >= 130)and(x <= 190)):
		x = (int)(x/2)

	elif(x <130):
		x = 0

	else:
		x = 31

# 75  83____98____113  121        servo
# 0   130___160___190  320        camera_x
# 0   65____80____95   320        camera_x / 2  
# 4b  53____62____71   79         hex

    for case in switch(x):
    	if case(0):
    		ser.write('\x4b'.encode())
    		break
    	if case(65):
    		ser.write('\x53'.encode())
    		break
    	if case(66):
    		ser.write('\x54'.encode())
    		break
    	if case(67):
    		ser.write('\x55'.encode())
    		break
    	if case(68):
    		ser.write('\x56'.encode())
    		break
    	if case(69):
    		ser.write('\x57'.encode())
    		break
    	if case(70):
    		ser.write('\x58'.encode())
    		break
    	if case(71):
    		ser.write('\x59'.encode())
    		break
    	if case(72):
    		ser.write('\x5a'.encode())
    		break
    	if case(73):
    		ser.write('\x5b'.encode())
    		break
    	if case(74):
    		ser.write('\x5c'.encode())
    		break
    	if case(75):
    		ser.write('\x5d'.encode())
    		break
    	if case(76):
    		ser.write('\x5e'.encode())
    		break
    	if case(77):
    		ser.write('\x5f'.encode())
    		break
    	if case(78):
    		ser.write('\x60'.encode())
    		break
    	if case(79):
    		ser.write('\x61'.encode())
    		break
    	if case(80):
    		ser.write('\x62'.encode())
    		break
    	if case(81):
    		ser.write('\x63'.encode())
    		break
    	if case(82):
    		ser.write('\x64'.encode())
    		break
    	if case(83):
    		ser.write('\x65'.encode())
    		break
    	if case(84):
    		ser.write('\x66'.encode())
    		break
    	if case(85):
    		ser.write('\x67'.encode())
    		break
    	if case(86):
    		ser.write('\x68'.encode())
    		break
    	if case(87):
    		ser.write('\x69'.encode())
    		break
    	if case(88):
    		ser.write('\x6a'.encode())
    		break
    	if case(89):
    		ser.write('\x6b'.encode())
    		break
    	if case(90):
    		ser.write('\x6c'.encode())
    		break
    	if case(91):
    		ser.write('\x6d'.encode())
    		break
    	if case(92):
    		ser.write('\x6e'.encode())
    		break
    	if case(93):
    		ser.write('\x6f'.encode())
    		break
    	if case(94):
    		ser.write('\x70'.encode())
    		break
    	if case(95):
    		ser.write('\x71'.encode())
    		break
    	if case(31):
    		ser.write('\x79'.encode())
    		break
    	if case():
    		break

if __name__ == "__main__":
	while(1):
		x_to_angle()