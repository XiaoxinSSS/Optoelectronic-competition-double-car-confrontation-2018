/*#-*- coding: utf-8 -*-
  # UART: pi  USBtoTTL to  arduino mega 2560
  # 2018.7.1 GS   */


#include<Servo.h>
Servo myservo;

unsigned int r;

void setup() 
{
  Serial.begin(9600);
  Serial2.begin(9600);
  myservo.attach(6);
  delay(20);
}

void uart_r()
{
  if(Serial2.available() > 0)
  {
    r = Serial2.read();
    Serial.println(r);
  }
}

void loop() 
{
  uart_r();
  myservo.write(r);
  delay(10);
}