/* coding = utf-8
   2018.7.3  GS 
   arduino mega 2560 2
*/

#include<Servo.h>
Servo myservo;

unsigned int r;
const int p_back_f = 46;
const int p_Servo_r_f = 48;
const int p_Servo_l_f = 50;
int back_f = 1,Servo_r_f = 1,Servo_l_f = 1;

void setup() 
{
  Serial.begin(9600);
  Serial2.begin(9600);
  pinMode(p_back_f,INPUT_PULLUP);
  pinMode(p_Servo_r_f,INPUT_PULLUP);
  pinMode(p_Servo_l_f,INPUT_PULLUP);
  myservo.attach(6);
  delay(20);
}

void read_f()
{
  back_f = digitalRead(p_back_f);
  Servo_r_f = digitalRead(p_Servo_r_f);
  Servo_l_f = digitalRead(p_Servo_l_f);
}

void uart_r()
{
  if(Serial2.available() > 0)
  {
    r = Serial2.read();
  }
}

void loop() 
{
  read_f();
  if(back_f == 0)
  {
    myservo.write(98);
    delay(50);
  }
  else if(Servo_r_f == 0)
  {
    myservo.write(121);
    delay(50);
  }
  else if(Servo_l_f == 0)
  {
    myservo.write(75);
    delay(50);
  }
  else
  {
    uart_r();
    myservo.write(r);
    delay(10);
  }
}


