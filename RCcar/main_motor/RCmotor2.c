/* coding = utf-8
   2018.7.5 GS 
   arduino mega 2560
*/

#include<Servo.h>
Servo motv;
Servo fServo;
Servo bServo;

const int p_start_m = 3;
const int max_f_v = 1630;
const int max_b_v = 1370;

const int mid_f_v = 1580;
const int mid_b_v = 1380;
 
const int min_f_v = 1570;
const int min_b_v = 1390;

const int  p_front1 = 22;
const int   p_back4 = 24;
const int p_f_down_v = 26;
const int p_b_down_v = 28;

const int p_fright2 = 30;
const int p_bright3 = 32;
const int  p_bleft5 = 34;
const int  p_fleft6 = 36;

const int p_back_f = 46;

const int p_Servo_r_f = 48;
const int p_Servo_l_f = 50;

const int p_yellf1 = 38;
const int p_yellf2 = 40;
const int p_yellb1 = 42;
const int p_yellb2 = 44;

int front1 = 1,fright2 = 1,bright3 = 1,back4 = 1,bleft5 = 1,fleft6 = 1;
int f_down_v = 1,b_down_v = 1;
int yellf1 = 1,yellf2 = 1,yellb1 = 1,yellb2 = 1;
int start_m = 1;
int ss = 0;
//int back_f = 0,Servo_r_f = 1,Servo_l_f = 1;

void setup()
{
  motv.attach(7);
  motv.writeMicroseconds(1500);
  Serial.begin(9600);
  
  pinMode(p_start_m,INPUT_PULLUP);
  
  pinMode(p_front1,INPUT_PULLUP);
  pinMode(p_fright2,INPUT_PULLUP);
  pinMode(p_bright3,INPUT_PULLUP);
  pinMode(p_back4,INPUT_PULLUP);
  pinMode(p_bleft5,INPUT_PULLUP);
  pinMode(p_fleft6,INPUT_PULLUP);

  pinMode(p_f_down_v,INPUT_PULLUP);
  pinMode(p_b_down_v,INPUT_PULLUP);

  pinMode(p_back_f,OUTPUT);

  pinMode(p_Servo_r_f,OUTPUT);
  pinMode(p_Servo_l_f,OUTPUT);
  
  digitalWrite(p_back_f,HIGH);
  digitalWrite(p_Servo_r_f,HIGH);
  digitalWrite(p_Servo_l_f,HIGH);

  pinMode(p_yellf1,INPUT_PULLUP);
  pinMode(p_yellf2,INPUT_PULLUP);
  pinMode(p_yellb1,INPUT_PULLUP);
  pinMode(p_yellb2,INPUT_PULLUP);
 
  fServo.attach(5);
  bServo.attach(6);

  fServo.write(90);
  bServo.write(90);
  
  delay(3000);
  
}

void f_servo_act()
{
  fServo.write(180);
  delay(400);
  fServo.write(90);
}

void b_servo_act()
{
  bServo.write(180);
  delay(400);
  bServo.write(90);
}

void get_quadrant()
{
  front1 = digitalRead(p_front1);
  fright2 = digitalRead(p_fright2);
  bright3 = digitalRead(p_bright3);
  back4 = digitalRead(p_back4);
  bleft5 = digitalRead(p_bleft5);
  fleft6 = digitalRead(p_fleft6);
}

void get_down_v()
{
  f_down_v = digitalRead(p_f_down_v);
  b_down_v = digitalRead(p_b_down_v);
}

void get_yell()
{
  yellf1 = digitalRead(p_yellf1);
  yellf2 = digitalRead(p_yellf2);
  yellb1 = digitalRead(p_yellb1);
  yellb2 = digitalRead(p_yellb2);
}

void front()
{
  motv.writeMicroseconds(max_f_v);
  //delay(5);
  get_down_v();
  Serial.println(f_down_v);
  if(f_down_v == 0)
  {
    motv.writeMicroseconds(1400);
    delay(500);
    while(1)
    {
      motv.writeMicroseconds(min_f_v);
      get_yell();
      if((yellf1 == 0)||(yellf1 == 0)||((yellf1 == 0)&&(yellf1 == 0)))
      {
        motv.writeMicroseconds(1450);
        delay(50);
        motv.writeMicroseconds(1500);
        f_servo_act();
        digitalWrite(p_back_f,LOW);
        motv.writeMicroseconds(mid_b_v);
        delay(500);
        motv.writeMicroseconds(1500);
        digitalWrite(p_back_f,HIGH);
        break;
      }
    }
  }
}

void back()
{
  //Serial.print(back4);
  motv.writeMicroseconds(max_b_v);
  get_down_v();
  //Serial.print(b_down_v);
  if(b_down_v == 0)
  {
    motv.writeMicroseconds(1600);
    delay(100);
    while(1)
    {
      motv.writeMicroseconds(min_b_v);
      get_yell();
      if((yellb1 == 0)||(yellb1 == 0)||((yellb1 == 0)&&(yellb1 == 0)))
      {
        motv.write(1500);
        b_servo_act();
        digitalWrite(p_back_f,LOW);
        motv.write(mid_f_v);
        delay(500);
        motv.write(1500);
        digitalWrite(p_back_f,HIGH);
        break;
      }
    }
  }
}

void fr()
{
  while(1)
  {
    motv.write(mid_b_v);
    digitalWrite(p_Servo_l_f,LOW);
    get_quadrant();
    if(front1 == 0)
    {
      motv.write(1500);
      digitalWrite(p_Servo_l_f,HIGH);
      break;
    }
  }
}

void fl()
{
  while(1)
  {
    motv.write(mid_b_v);
    digitalWrite(p_Servo_r_f,LOW);
    get_quadrant();
    if(front1 == 0)
    {
      motv.write(1500);
      digitalWrite(p_Servo_r_f,HIGH);
      break;
    }
  }
}

void br()
{
  while(1)
  {
    motv.write(mid_f_v);
    digitalWrite(p_Servo_l_f,LOW);
    get_quadrant();
    if(back4 == 0)
    {
      motv.write(1500);
      digitalWrite(p_Servo_l_f,HIGH);
      break;
    }
  }
}

void bl()
{
  while(1)
  {
    motv.write(mid_f_v);
    digitalWrite(p_Servo_r_f,LOW);
    get_quadrant();
    if(back4 == 0)
    {
      motv.write(1500);
      digitalWrite(p_Servo_r_f,HIGH);
      break;
    }
  }
}

void loop()
{
  start_m = digitalRead(p_start_m);
  Serial.println(start_m);
  if(start_m == 0)
  {
    while(1)
    {
      get_quadrant();
      ss = front1 * 100000 + fright2 * 10000 + bright3 * 1000 + back4 * 100 + bleft5 * 10 + fleft6;
//      Serial.print(front1);
//      Serial.print(fright2);
//      Serial.print(bright3);
//      Serial.print(p_back4);
//      Serial.print(bleft5);
//      Serial.println(fleft6);
        Serial.println(ss);
//      if((((front1 == 0)&&(fright2 == 1))||((front1 == 0)&&(fright2 == 0)))&&(bright3 == 1)&&(back4 == 1)&&(bleft5 == 1)&&(fleft6 == 1))
//      {
//        front();
//      }
//      else if((front1 == 1)&&(fright2 == 1)&&(bright3 == 1)&&(back4 == 0)&&(bleft5 == 1)&&(fleft6 == 1))
//      { 
//        back();
//      }
//      else if((front1 == 1)&&(fright2 == 0)&&(bright3 == 1)&&(back4 == 1)&&(bleft5 == 1)&&(fleft6 == 1))
//      {
//        fr();
//      }
//      else if((front1 == 1)&&(fright2 == 1)&&(bright3 == 1)&&(back4 == 1)&&(bleft5 == 1)&&(fleft6 == 0))
//      {
//        fl();
//      }
//      else if((front1 == 1)&&(fright2 == 1)&&(bright3 == 0)&&(back4 == 1)&&(bleft5 == 1)&&(fleft6 == 1))
//      {
//        br();
//      }
//      else if((front1 == 1)&&(fright2 == 1)&&(bright3 == 1)&&(back4 == 1)&&(bleft5 == 0)&&(fleft6 == 1))
//      {
//        bl();
//      }
  //motv.writeMicroseconds(160);
      if((ss == 011111)||(ss == 001111)||(ss == 011110))
      {
        front();
      }
      else if((ss == 111011)||(ss == 110011)||(ss == 111001))
      {
        back();
      }
      else if(ss == 101111)
      {
        fr();
      }
      else if(ss == 111110)
      {
        fl();
      }
      else if(ss == 110111)
      {
        br();
      }
      else if(ss == 111101)
      {
        bl();
      }
    }
  }
}
