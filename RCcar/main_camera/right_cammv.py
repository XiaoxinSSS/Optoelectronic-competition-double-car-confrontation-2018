import pyb
from pid import PID
from turn import TURN
from pyb import Pin
import sensor, image, time
GRAYSCALE_1 = (220, 255)
roi = (60,80,260,160)
sensor.reset()
sensor.set_pixformat(sensor.GRAYSCALE)
sensor.set_framesize(sensor.QVGA)
sensor.set_windowing(roi)
sensor.skip_frames( time = 1000)
sensor.set_auto_whitebal(False)
out_1 = Pin('P0', Pin.OUT_PP)
out_2 = Pin('P1', Pin.OUT_PP)
out_1.low()
out_2.low()
while(True):
    img = sensor.snapshot()
    #img = img.binary([GRAYSCALE_1])
    blobs = img.find_blobs([GRAYSCALE_1])
    if blobs:
        pixs = []
        for blob in blobs:
            pixs.append(blob[4])
        mx = max(pixs)
        inde = pixs.index(mx)
        length_1 = blobs[inde][6]
        print(blobs[inde][5])
        if blobs[inde][5]<=130:
            out_1.high()
            out_2.low()
        else:
            out_1.low()
            out_2.high()
    else:
        print('Not have!')
        out_1.high()
        out_2.high()
